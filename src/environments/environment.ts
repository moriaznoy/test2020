// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: true,
  firebaseConfig: {
    apiKey: "AIzaSyAl6Ast4THzOepdqmXCeNqIEezJCaQI_TE",
    authDomain: "test2020-5b393.firebaseapp.com",
    databaseURL: "https://test2020-5b393.firebaseio.com",
    projectId: "test2020-5b393",
    storageBucket: "test2020-5b393.appspot.com",
    messagingSenderId: "872354740914",
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
