import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ClassifyService } from '../classify.service';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-classify',
  templateUrl: './classify.component.html',
  styleUrls: ['./classify.component.css']
})
export class ClassifyComponent implements OnInit {
  classCategory:string = "Loading..";
  categoryImage:string;

  id: string;
  userId:string;

  constructor(public classervice:ClassifyService, public auth:AuthService) { }

  ngOnInit() 
  {
    this.auth.user.subscribe(
      user => {
        this.userId = user.uid;
        console.log(this.userId);
    })
    this.classervice.classify().subscribe(res => 
      {
        this.classCategory = this.classervice.categories[res];
        console.log(this.classCategory);
        }   
    );
 
}
  

  addToFirebase(category:string, document:string)
  {
    this.classervice.addDocu(this.userId,category,document);
  }
}