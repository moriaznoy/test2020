import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ClassifyService {


  //הגדרת משתנה מסוג "אוסף"  י 
  
  private url = "https://rniqbt3fbh.execute-api.us-east-1.amazonaws.com/beta";
  public categories:object = {0: 'business', 1: 'entertainment', 2: 'politics', 3: 'sport', 4: 'tech'};
  public doc:string;
  
  userCollection:AngularFirestoreCollection= this.db.collection('users');
  docuCollection:AngularFirestoreCollection;

  addDocu(userId:string, category:string,document:string){
    const article = {category:category,document:document};
    //this.db.collection('books').add(book);
    this.userCollection.doc(userId).collection('article').add(article); //הוספה של הספר בתוך האוסף של היוזרים
    this.router.navigate(['/usercoll']);
  }


  classify():Observable<any>{
    let json = {
      "articles":[
        {"text":this.doc}
      ]
    }
    let body = JSON.stringify(json);
    return this.http.post<any>(this.url,body).pipe(
      map(res =>{
        let final = res.body.replace('[','')
        final = final.replace(']','')
        return final; 
      })
    )
  }

  getDocu(userId:string):Observable<any[]>{
    //return this.db.collection('books').valueChanges({idField:'id'}); 
    this.docuCollection = this.db.collection(`users/${userId}/article`);
    return this.docuCollection.snapshotChanges().pipe(
      map(
       collection => collection.map(
         document=> {
           const data = document.payload.doc.data();
           data.id = document.payload.doc.id;
           return data;
         }
       ) 
      )
    )
   }

   constructor(private http:HttpClient, private db:AngularFirestore ,  private router:Router) { }

}