import { Component, OnInit } from '@angular/core';
import { ClassifyService } from '../classify.service';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-formclass',
  templateUrl: './formclass.component.html',
  styleUrls: ['./formclass.component.css']
})
export class FormclassComponent implements OnInit {

  text:string;
  
  constructor(private classify:ClassifyService,public auth:AuthService, private router:Router) { }

  userId:string;

  ngOnInit() {
    this.auth.user.subscribe(
      user => {
        this.userId = user.uid;
        console.log(this.userId);
    })
  }
  onSubmit(){
    this.classify.doc = this.text;
    this.router.navigate(['/classify']);
  }


}
